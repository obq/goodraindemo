FROM alpine:3.8

WORKDIR root

RUN set -ex \
    && echo "http://mirrors.ustc.edu.cn/alpine/v3.8/main/" > /etc/apk/repositories \
    && apk upgrade \
    && apk add bash curl \
	&& apk add --no-cache libsodium py2-pip \
    && pip --no-cache-dir install https://github.com/gitugser/dumpls/archive/master.zip \
    && rm -rf /var/cache/apk/*

ADD config.json ~/config.json

EXPOSE 443
#EXPOSE 443 443/udp

ENTRYPOINT ["dsserver", "-c", "~/config.json"]
